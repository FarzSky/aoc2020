import utils.fileutil as fileutil

fields = {
    "byr": [True, 4, [1920, 2002]],
    "iyr": [True, 4, [2010, 2020]],
    "eyr": [True, 4, [2020, 2030]],
    "hgt": [True, {"cm": [150, 193], "in": [59, 76]}],
    "hcl": [True, "#", 6, ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f']],
    "ecl": [True, ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]],
    "pid": [True, 9, ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']],
    "cid": [False]
}

def p1(data):
    invalids = 0
    for batch in data:
        keys = [x.split(":")[0] for x in batch]
        for field in fields:
            if not check_exists(keys, field, fields[field][0]):
                invalids += 1
                break

    return len(data) - invalids

def p2(data):
    invalids = 0
    for batch in data:
        keys = [x.split(":")[0] for x in batch]
        invalid = False
        for field in fields:
            if not check_exists(keys, field, fields[field][0]):
                invalids += 1
                invalid = True
                break

        if invalid:
            continue

        for item in batch:
            if not check_rule(item):
                invalids += 1
                break

    return len(data) - invalids

def check_rule(item):
    key_value = item.split(":")
    if key_value[0] == "byr":
        return check_str_len(key_value[1], fields[key_value[0]][1]) and check_digit_range(key_value[1], fields[key_value[0]][2])
    elif key_value[0] == "iyr":
        return check_str_len(key_value[1], fields[key_value[0]][1]) and check_digit_range(key_value[1], fields[key_value[0]][2])
    elif key_value[0] == "eyr":
        return check_str_len(key_value[1], fields[key_value[0]][1]) and check_digit_range(key_value[1], fields[key_value[0]][2])
    elif key_value[0] == "hgt":
        return check_hgt(key_value[1], fields[key_value[0]][1])
    elif key_value[0] == "hcl":
        return check_start_with(key_value[1], fields[key_value[0]][1]) and check_str_len(key_value[1].replace("#", ""), fields[key_value[0]][2]) \
               and check_contains(list(key_value[1].replace("#", "")), fields[key_value[0]][3])
    elif key_value[0] == "ecl":
        return check_contains([key_value[1]], fields[key_value[0]][1])
    elif key_value[0] == "pid":
        return check_str_len(key_value[1], fields[key_value[0]][1]) \
               and check_contains(list(key_value[1]), fields[key_value[0]][2])
    elif key_value[0] == "cid":
        return True
    else:
        return False

def check_exists(keys, keyToCheck, is_required=True):
    return keyToCheck in keys or not is_required

def check_digit_range(data, range):
    value = int(data)
    return value >= range[0] and value <= range[1]

def check_hgt(data, rule):
    for charEndsWith in rule:
        if data.endswith(charEndsWith):
            hgt = data[0:len(data) - len(charEndsWith)]
            return check_digit_range(hgt, rule[charEndsWith])
    return False

def check_start_with(data, char):
    return data.startswith(char)

def check_str_len(data, char_count):
    return len(data) == char_count

def check_contains(data, checklist):
    for d in data:
        if d not in checklist:
            return False
    return True

if __name__ == '__main__':
    data = fileutil.read_batches()
    print("Part 1 answer: " + str(p1(data)))
    print("Part 2 answer: " + str(p2(data)))