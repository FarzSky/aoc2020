def read(path="input.txt"):
    with open(path, 'r') as f:
        inp = f.read()
    return inp

def readlines(path="input.txt"):
    with open(path, 'r') as f:
        inp = f.read().splitlines()
    return inp

def readlines_int(path="input.txt"):
    with open(path, 'r') as f:
        inp = f.read().splitlines()
    return [int(x) for x in inp]

def readmatrix(path="input.txt"):
    with open(path, 'r') as f:
        inp = [list(line) for line in f.read().splitlines()]
    return inp

def read_batches(path="input.txt", batch_spliter='\n'):
    data = read(path).split(batch_spliter + '\n')
    return [x.replace('\n', ' ').split(' ') for x in data]