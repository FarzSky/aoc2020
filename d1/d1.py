import utils.fileutil as fileutil

def p1(data):
    data.sort(reverse=True)

    x = 0
    y = 0
    found = False
    for i in range(len(data) - 1):
        for j in range(i + 1, len(data)):
            sum = data[i] + data[j]
            if sum == 2020:
                x = data[i]
                y = data[j]
                found = True
                break
            elif sum < 2020:
                break
        if found:
            break

    return x * y

def p2(data):
    x2 = 0
    y2 = 0
    z2 = 0
    found = False
    for i in range(len(data) - 2):
        for j in range(i + 1, len(data) - 1):
            tempsum = data[i] + data[j]
            if (tempsum > 2020):
                break
            for k in range(j + 1, len(data)):
                sum = tempsum + data[k]
                if sum == 2020:
                    x2 = data[i]
                    y2 = data[j]
                    z2 = data[k]
                    found = True
                    break
                elif sum < 2020:
                    break
            if found:
                break
        if found:
            break

    return x2 * y2 * z2

if __name__ == '__main__':
    data = fileutil.readlines_int("input.txt")
    print("Part 1 answer: " + str(p1(data)))
    print("Part 2 answer: " + str(p2(data)))