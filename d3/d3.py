import math
import utils.fileutil as fileutil

def p1(data):
    return traversing_count(data, 3, 1)

def p2(data):
    return math.prod(traversing_count(data, dx, dy)
             for dx, dy in [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
         )

def traversing_count(data, dx, dy):
    return sum(line[(i * dx) % len(line)] == "#"
               for i, line in enumerate(data[::dy])
           )

    if __name__ == '__main__':
        data = fileutil.readmatrix("input.txt")
        print("Part 1 answer: " + str(p1(data)))
        print("Part 2 answer: " + str(p2(data)))