import utils.fileutil as fileutil

def parse_codes(data):
    passwords = []
    for rule in data:
        min = int(rule.split("-")[0])
        max = int(rule.split("-")[1].split(" ")[0])
        letter = rule.split(" ")[1].replace(":", "")
        password = rule.split(" ")[2]
        passwords.append([min, max, letter, password])

    return passwords

def p1(data):
    passwords = parse_codes(data)

    valid = 0
    for passwordRule in passwords:
        countLetters = 0
        for letter in list(passwordRule[3]):
            if letter == passwordRule[2]:
                countLetters += 1
        if countLetters >= passwordRule[0] and countLetters <= passwordRule[1]:
            valid += 1

    return valid

def p2(data):
    passwords = parse_codes(data)

    valid = 0
    for passwordRule in passwords:
        char1 = passwordRule[3][passwordRule[0] - 1]
        char2 = passwordRule[3][passwordRule[1] - 1]
        if (bool(char1 == passwordRule[2]) ^ bool(char2 == passwordRule[2])):
            valid += 1

    return valid

if __name__ == '__main__':
    data = fileutil.readlines("input.txt")
    print("Part 1 answer: " + str(p1(data)))
    print("Part 2 answer: " + str(p2(data)))