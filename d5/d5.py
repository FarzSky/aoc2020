import utils.fileutil as fileutil

def p1(data):
    seat_ids = [seat_id(*decode_seat(seat)) for seat in data]
    return max(seat_ids)

def p2(data):
    seat_ids = [seat_id(*decode_seat(seat)) for seat in data]
    seat_ids.sort()
    for seat_id_index in range(1, len(seat_ids) - 1):
        if seat_ids[seat_id_index + 1] - seat_ids[seat_id_index] != 1:
            return seat_ids[seat_id_index] + 1

def filter_seats(variable):
    return variable < 800 and variable > 200

def decode_seat(seat_code):
    row_code = seat_code[:7]
    column_code = seat_code[7:10]

    row_code = row_code.replace("B", "1")
    row_code = row_code.replace("F", "0")

    column_code = column_code.replace("R", "1")
    column_code = column_code.replace("L", "0")

    return [int(row_code, 2), int(column_code, 2)]

def seat_id(row, column):
    return row * 8 + column

if __name__ == '__main__':
    data = fileutil.readlines()
    print("Part 1 answer: " + str(p1(data)))
    print("Part 2 answer: " + str(p2(data)))


