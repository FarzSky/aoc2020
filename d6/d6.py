import utils.fileutil as fileutil

def p1(data):
    yeses = 0
    for group in data:
        group_people = "".join(map(str, group))
        yes_answers = set(group_people)
        yeses += len(yes_answers)
    return yeses

def p2(data):
    yeses = 0
    for group in data:
        first_guy = set(group[0])
        for i in range(0, len(group)):
            first_guy = first_guy.intersection(set(group[i]))
        yeses += len(first_guy)
    return yeses

if __name__ == '__main__':
    data = fileutil.read_batches()
    print("Part 1 answer: " + str(p1(data)))
    print("Part 2 answer: " + str(p2(data)))


